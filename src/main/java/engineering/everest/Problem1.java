package engineering.everest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import engineering.everest.helpers.EverestHelper;
import engineering.everest.helpers.EverestPackage;

public class Problem1 {

	public static void main(String[] args) throws Exception {
		Problem1 p1 = new Problem1();
		p1.start();
	}
	
	public void start() throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter base delivery cost and number of packages in the same format as given in problem statement 1");
		String baseDeliveryCostAndNumPackages = br.readLine();
		int numPackages = EverestHelper.getNumPackages(baseDeliveryCostAndNumPackages);
		float baseDeliveryCost = EverestHelper.getBaseDeliveryCost(baseDeliveryCostAndNumPackages);
		
		List<EverestPackage> everestPackages = new ArrayList<EverestPackage>();
		for(int i=1; i<=numPackages; i++) {
			String packageInfo = br.readLine();
			EverestPackage pkg = EverestHelper.buildPackage(packageInfo, baseDeliveryCost);
			everestPackages.add(pkg);
		}
		printOutput(everestPackages);
	}
	
	public void printOutput(List<EverestPackage> packages) {
		System.out.println("\n===========");
		System.out.println("Your costs are:\n");
		for(EverestPackage everestPackage: packages) {
			System.out.println(EverestHelper.getPackageCostOutput(everestPackage));
		}
	}
}
