package engineering.everest.helpers;

public class EverestPackage {
	final int PACKAGE_WEIGHT_COST_MULTIPLIER = 10;
	final int PACKAGE_DISTANCE_COST_MULTIPLIER = 5;
	
	String packageName;
	float packageWeightInKg;
	float packageDistanceInKm;
	float baseDeliveryCost;
	String packageOfferCode;
	float packageDiscount;
	float packageCost;
	
	public EverestPackage(String packageName, 
			float packageWeightInKg, 
			float packageDistance, 
			float baseDeliveryCost,
			String packageOfferCode) {
		
		this.packageName = packageName;
		this.packageWeightInKg = packageWeightInKg;
		this.packageDistanceInKm = packageDistance;
		this.baseDeliveryCost = baseDeliveryCost;
		this.packageOfferCode = packageOfferCode;
		calculateDiscountAndCost();
	}
	
	public String getPackageOfferCode() {
		return this.packageOfferCode;
	}
	
	public float getPackageWeight() {
		return packageWeightInKg;
	}
	
	public String getPackageName() {
		return packageName;
	}
	
	public float getPackageDistance() {
		return packageDistanceInKm;
	}
	
	public float getPackageDiscount() {
		return packageDiscount;
	}
	
	public float getPackageCost() {
		return packageCost;
	}
	
	private void calculateDiscountAndCost() {
		Offer offer = Offers.get(packageOfferCode);
		float cost = baseDeliveryCost + 
				(packageWeightInKg * PACKAGE_WEIGHT_COST_MULTIPLIER) + 
				(packageDistanceInKm * PACKAGE_DISTANCE_COST_MULTIPLIER);
		
		if (offer == null) {
			this.packageDiscount = 0;
			this.packageCost = cost;
		}
		else {
			if(weightWithinOfferCriteria(packageWeightInKg, offer) && distanceWithinOfferCriteria(packageDistanceInKm, offer)){
				this.packageDiscount = cost * (offer.percentDiscount/100);
				this.packageCost = cost - this.packageDiscount;
			}
			else {
				this.packageDiscount = 0;
				this.packageCost = cost;
			}
		}
	}
	
	private boolean weightWithinOfferCriteria(float packageWeightInKg, Offer offer) {
		if(packageWeightInKg <= offer.maxWeightCriterion && packageWeightInKg >= offer.minWeightCriterion) {
			return true;
		}
		return false;
	}
	
	private boolean distanceWithinOfferCriteria(float packageDistanceInKm, Offer offer) {
		if(packageDistanceInKm <= offer.maxDistanceCriterion && packageDistanceInKm >= offer.minDistanceCriterion) {
			return true;
		}
		return false;
	}
}