package engineering.everest.helpers;

public class Vehicle {
	int vehicleId;
	float nextAvailableAtHours;
	
	public Vehicle(int vehicleId, float nextAvailableAtHours) {
		this.vehicleId = vehicleId;
		this.nextAvailableAtHours = nextAvailableAtHours;
	}
	
	public float getNextAvailableAtHours() {
		return this.nextAvailableAtHours;
	}
	
	public void setNextAvailableAtHours(float nextAvailableHours) {
		this.nextAvailableAtHours = nextAvailableHours;
	}
}
