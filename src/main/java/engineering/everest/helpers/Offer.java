package engineering.everest.helpers;

public class Offer {
	String offerCode;
	float percentDiscount;
	float minDistanceCriterion;
	float maxDistanceCriterion;
	float minWeightCriterion;
	float maxWeightCriterion;
	
	public Offer(
			String offerCode, 
			float percentDiscount, 
			float minDistanceCriterion, 
			float maxDistanceCriterion,
			float minWeightCriterion,
			float maxWeightCriterion) {
		this.offerCode = offerCode;
		this.percentDiscount = percentDiscount;
		this.minDistanceCriterion = minDistanceCriterion;
		this.maxDistanceCriterion = maxDistanceCriterion;
		this.minWeightCriterion = minWeightCriterion;
		this.maxWeightCriterion = maxWeightCriterion;
	}
}
