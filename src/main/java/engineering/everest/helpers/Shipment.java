package engineering.everest.helpers;

import java.util.List;

public class Shipment {
	
	int shipmentId;
	List<EverestPackage> packages;
	float shipmentWeight;
	float maxPackageDistance;
	
	public Shipment(int shipmentId, List<EverestPackage> packages) {
		this.shipmentId = shipmentId;
		this.packages = packages;
		this.shipmentWeight = getShipmentWeight(packages);
		this.maxPackageDistance = getMaxPackageDistance(packages);
	}
	
	public float getShipmentWeight() {
		return this.shipmentWeight;
	}
	
	public float getMaxPackageDistance() {
		return this.maxPackageDistance;
	}
	
	public List<EverestPackage> getPackages() {
		return this.packages;
	}
	
	private float getShipmentWeight(List<EverestPackage> packages) {
		float shipmentWeight = 0;
		for(EverestPackage pkg: packages) {
			shipmentWeight += pkg.getPackageWeight();
		}
		return shipmentWeight;
	}
	
	private float getMaxPackageDistance(List<EverestPackage> packages) {
		float maxPackageDistance = Float.NEGATIVE_INFINITY;
		for(EverestPackage pkg: packages) {
			if(pkg.getPackageDistance() > maxPackageDistance) {
				maxPackageDistance = pkg.getPackageDistance();
			}
		}
		return maxPackageDistance;
	}
}