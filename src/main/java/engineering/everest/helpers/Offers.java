package engineering.everest.helpers;

import java.util.HashMap;

public class Offers {
	
	 private static HashMap<String, Offer> offerMap = new HashMap<String, Offer>() {{
		put("OFR001", new Offer("OFR001", 10, 0, 199, 70, 200)); //Distance should be 0 to 199
		put("OFR002", new Offer("OFR002", 7, 50, 150, 100, 250));
		
		/*The below code is not present in PDF document. However, the example in problem 2
		 * on page 11 assumes it is present. Hence, adding it here.*/
		put("OFFR002", new Offer("OFFR002", 7, 50, 150, 100, 250));
		put("OFR003", new Offer("OFR003", 5, 50, 250, 10, 150));
	}};
	
	public static Offer get(String offerCode) {
		return offerMap.get(offerCode);
	}
}
