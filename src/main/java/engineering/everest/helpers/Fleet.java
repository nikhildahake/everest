package engineering.everest.helpers;

public class Fleet {
	int numOfVehicles;
	float maxSpeed;
	float maxWeight;
	
	public Fleet(int numOfVehicles, float maxSpeed, float maxWeight) {
		this.numOfVehicles = numOfVehicles;
		this.maxSpeed = maxSpeed;
		this.maxWeight = maxWeight;
	}
	
	public int getNumOfVehicles() {
		return numOfVehicles;
	}
	
	public float getMaxSpeed() {
		return maxSpeed;
	}
	
	public float getMaxWeight() {
		return maxWeight;
	}
}
