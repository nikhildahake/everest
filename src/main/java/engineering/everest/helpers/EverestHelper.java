package engineering.everest.helpers;

import java.text.DecimalFormat;
import java.util.HashMap;

public class EverestHelper {
	
	private final DecimalFormat df = new DecimalFormat("0.00");
	
	public static float getBaseDeliveryCost(String baseDeliveryCostAndNumPackages) {
		//String format is: 100 3
		String parts[] = baseDeliveryCostAndNumPackages.split(" ");
		float baseDeliveryCost = Float.parseFloat(parts[0]);
		return baseDeliveryCost;
	}
	
	public static int getNumPackages(String baseDeliveryCostAndNumPackages) {
		String parts[] = baseDeliveryCostAndNumPackages.split(" ");
		int numPackages = Integer.parseInt(parts[1]);
		return numPackages;
	}
	
	public static EverestPackage buildPackage(String packageInfo, float packageBaseDeliveryCost) {
		//packageInfo format is: PKG1 5 5 OFR001
		String parts[] = packageInfo.split(" ");
		
		return new EverestPackage(parts[0], 
				Float.parseFloat(parts[1]), 
				Float.parseFloat(parts[2]), 
				packageBaseDeliveryCost, 
				parts[3]);
	}
	
	public static String getPackageCostOutput(EverestPackage everestPackage) {
		return everestPackage.getPackageName() + " " + everestPackage.getPackageDiscount() + " " + everestPackage.getPackageCost();
	}
	
	public static Fleet getFleet(String fleetDetails) {
		//String format is: 2 70 200
		String parts[] = fleetDetails.split(" ");
		return new Fleet(Integer.parseInt(parts[0]), Float.parseFloat(parts[1]), Float.parseFloat(parts[2]));
	}
	
	public static String getPackageCostAndDeliveryTimeOutput(EverestPackage everestPackage, HashMap<String, Float> packageDeliveryTimes) {
		return everestPackage.getPackageName() + 
				" " + 
				everestPackage.getPackageDiscount() + 
				" " + 
				everestPackage.getPackageCost() + 
				" " +
				packageDeliveryTimes.get(everestPackage.getPackageName());
	}
}