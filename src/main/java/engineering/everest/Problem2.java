package engineering.everest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import engineering.everest.helpers.EverestHelper;
import engineering.everest.helpers.EverestPackage;
import engineering.everest.helpers.Fleet;

public class Problem2 {

	public static void main(String[] args) throws Exception {
		Problem2 p2 = new Problem2();
		EverestLogistics logistics = new EverestLogistics();
		p2.start(logistics);
	}
	
	public void start(EverestLogistics logistics) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter base delivery cost and number of packages \n in the same format as given in problem statement 2 \n"
				+ "followed by fleet details.");
		String baseDeliveryCostAndNumPackages = br.readLine();
		int numPackages = EverestHelper.getNumPackages(baseDeliveryCostAndNumPackages);
		float baseDeliveryCost = EverestHelper.getBaseDeliveryCost(baseDeliveryCostAndNumPackages);
		
		List<EverestPackage> packages = new ArrayList<EverestPackage>();
		
		for(int i=1; i<=numPackages; i++) {
			String packageDetails = br.readLine();
			EverestPackage pkg = EverestHelper.buildPackage(packageDetails, baseDeliveryCost);
			packages.add(pkg);
		}
		String fleetDetails = br.readLine();
		Fleet fleet = EverestHelper.getFleet(fleetDetails);
		printOutput(logistics, packages, fleet);
	}
	
	public void printOutput(EverestLogistics logistics, List<EverestPackage> packages, Fleet fleet) {
		List<EverestPackage> allPackages = new ArrayList<>(packages);
		HashMap<String, Float> packageIdDeliveryTimes = logistics.getPackageDeliveryTime(packages, fleet);
		System.out.println("\n===========");
		System.out.println("Your cost and delivery times are:\n");
		for(EverestPackage everestPackage: allPackages) {
			System.out.println(EverestHelper.getPackageCostAndDeliveryTimeOutput(everestPackage, packageIdDeliveryTimes));
		}
	}
}