package engineering.everest;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import engineering.everest.helpers.EverestPackage;
import engineering.everest.helpers.Fleet;
import engineering.everest.helpers.Shipment;
import engineering.everest.helpers.Vehicle;

public class EverestLogistics {
	
	private final DecimalFormat df = new DecimalFormat("0.00");
	
	public HashMap<String, Float> getPackageDeliveryTime(List<EverestPackage> packages, Fleet fleet) {
		/*The problem statement rounds examples down to 2 decimal places.
		 * */
		df.setRoundingMode(RoundingMode.DOWN);
		
		HashMap<String, Float> packageDeliveryTimes = new HashMap<String, Float>();
		List<Vehicle> vehicles = initializeFleet(fleet);
		while(packages.size()!=0) {
			int maxNumPackagesInShipment = getMaxNumOfPackagesInShipment(packages, fleet);
			Shipment shipment = buildOptimalShipment(maxNumPackagesInShipment, packages, fleet);
			sortVehiclesByNextAvailableAtASC(vehicles);

			float maxDistanceForShipment = shipment.getMaxPackageDistance();
			float nextAvailableHours = vehicles.get(0).getNextAvailableAtHours();
			float timeForRoundTrip = Float.parseFloat(df.format(maxDistanceForShipment/fleet.getMaxSpeed())) * 2;
			vehicles.get(0).setNextAvailableAtHours(nextAvailableHours + timeForRoundTrip);
			
			for (EverestPackage pkg: shipment.getPackages()) {
				float deliveryTime = nextAvailableHours + Float.parseFloat(df.format(pkg.getPackageDistance()/fleet.getMaxSpeed())); 
				packageDeliveryTimes.put(pkg.getPackageName(), deliveryTime);
			}
			
			//reduce number of remaining packages
			packages.removeAll(shipment.getPackages());
		}
		
		return packageDeliveryTimes;
	}
	
	private void sortVehiclesByNextAvailableAtASC(List<Vehicle> vehicles) {
		Collections.sort(vehicles, new Comparator<Vehicle>(){
			public int compare(Vehicle v1, Vehicle v2){
				   if (v1.getNextAvailableAtHours() > v2.getNextAvailableAtHours()) {
					   return 1;
				   }else if (v1.getNextAvailableAtHours() == v2.getNextAvailableAtHours()) {
					   return 0;
				   }else {
					   return -1;
				   }
			   }
			});
	}
	
	private List<Vehicle> initializeFleet(Fleet fleet) {
		List<Vehicle> vehicles = new ArrayList<Vehicle>();
		for (int i=1; i<=fleet.getNumOfVehicles(); i++) {
			Vehicle vehicle = new Vehicle(i, 0);
			vehicles.add(vehicle);
		}
		return vehicles;
	}
	
	private Shipment buildOptimalShipment(
			int maxNumPackagesInShipment, 
			List<EverestPackage> packages,
			Fleet fleet) {
		packages = sortPackagesByWeightDesc(packages);

		List<EverestPackage> shipment = new ArrayList<EverestPackage>();
		HashMap<Integer, List<EverestPackage>> shipmentHM = new HashMap<Integer, List<EverestPackage>>();
		int numPossibleShipments = 0;
		
		/*
		 * Assumption: A package weight will always be less than maximum weight that
		 * can be delivered by a fleet vehicle.
		 * */
		
		outer: for (int i=0; i<packages.size(); i++) {
				shipment = new ArrayList<EverestPackage>();
				shipment.add(packages.get(i));
				float shipmentWeight = packages.get(i).getPackageWeight();
				int numPackages = 1;
				
				if (numPackages == maxNumPackagesInShipment) {
					/*One shipment found - store it. There could be multiple shipments
					 * with same number of packages. 
					 */
					numPossibleShipments++;
					shipmentHM.put(numPossibleShipments, shipment);
					
					/*
					 * No point in looking for more packages because
					 * we have already reached maximum number of packages for 
					 * this shipment. i.e 1
					 * 
					 * This is the heaviest package because packages
					 * are already sorted in descending order. We cannot get
					 * a package heavier than this. So, break this loop*/
					continue outer;
				}
				
				for(int j=i+1; j<packages.size(); j++) {
					if (numPackages == maxNumPackagesInShipment) {
						/*One shipment found - store it. There could be multiple shipments
						 * with same number of packages. 
						 */
						numPossibleShipments++;
						shipmentHM.put(numPossibleShipments, shipment);
						continue outer;
					}
					
					if (shipmentWeight + packages.get(j).getPackageWeight() <= fleet.getMaxWeight()) {
						shipmentWeight = shipmentWeight + packages.get(j).getPackageWeight();
						shipment.add(packages.get(j));
						numPackages++;
					}
				}
			}
		
		return getShipmentToShip(getPossibleShipments(shipmentHM));
	}
	
	private List<Shipment> getPossibleShipments(HashMap<Integer, List<EverestPackage>> shipmentHM) {
		List<Shipment> possibleShipments = new ArrayList<Shipment>();
		Iterator<Integer> keys = shipmentHM.keySet().iterator();
		
		while(keys.hasNext()) {
			int key = keys.next();
			List<EverestPackage> packages = shipmentHM.get(key);
			Shipment shipment = new Shipment(key, packages);
			possibleShipments.add(shipment);
		}
		
		return possibleShipments;
	}
	
	private Shipment getShipmentToShip(List<Shipment> shipments) {
		/*There could be multiple shipments with same number of packages. 
		 * Try to maximize for shipment weight.
		 * If two, shipments have same weight, try to minimize for delivery
		 * time of the shipment.
		 * */
		
		sortShipmentsByWeightDesc(shipments);
		if(shipments.size() >=2 && shipments.get(0).getShipmentWeight() == shipments.get(1).getShipmentWeight()) {
			sortShipmentsByShortestDeliveryTime(shipments);
		}
		return shipments.get(0);
	}
	
	private int getMaxNumOfPackagesInShipment(List<EverestPackage> packages, Fleet fleet) {
		packages = sortPackagesByWeightAsc(packages);
		int maxNumPackagesInShipment = 0;
		float shipmentWeight = 0;
		int i=0;
		do {
			float packageWeight = packages.get(i).getPackageWeight();
			if (shipmentWeight + packageWeight <= fleet.getMaxWeight()) {
				maxNumPackagesInShipment++;
				shipmentWeight = shipmentWeight + packageWeight;
				i++;
			}
			else {
				break;
			}
		} while(shipmentWeight < fleet.getMaxWeight() && i<packages.size());
		
		return maxNumPackagesInShipment;
	}
	
	private List<EverestPackage> sortPackagesByWeightAsc(List<EverestPackage> packages) {
		packages.sort(Comparator.comparing(EverestPackage::getPackageWeight));
		return packages;
	}
	
	private List<EverestPackage> sortPackagesByWeightDesc(List<EverestPackage> packages) {
		packages.sort(Comparator.comparing(EverestPackage::getPackageWeight, Comparator.reverseOrder()));
		return packages;
	}
	
	private List<Shipment> sortShipmentsByWeightDesc(List<Shipment> shipments) {
		shipments.sort(Comparator.comparing(Shipment::getShipmentWeight, Comparator.reverseOrder()));
		return shipments;
	}
	
	private List<Shipment> sortShipmentsByShortestDeliveryTime(List<Shipment> shipments) {
		shipments.sort(Comparator.comparing(Shipment::getMaxPackageDistance));
		return shipments;
	}
}