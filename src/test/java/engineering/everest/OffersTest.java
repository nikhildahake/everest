package engineering.everest;

import engineering.everest.helpers.Offer;
import engineering.everest.helpers.Offers;
import junit.framework.TestCase;

public class OffersTest extends TestCase{
	public void testGet() throws Exception {
		Offer offer = Offers.get("OFR001");
		assertTrue(offer instanceof Offer);
	}
}