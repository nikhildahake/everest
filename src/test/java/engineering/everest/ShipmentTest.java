package engineering.everest;

import java.util.ArrayList;
import java.util.List;

import engineering.everest.helpers.EverestHelper;
import engineering.everest.helpers.EverestPackage;
import engineering.everest.helpers.Shipment;
import junit.framework.TestCase;

public class ShipmentTest extends TestCase{
	
	public void testGetShipmentWeight() throws Exception {
		List<String> sampleInput = new ArrayList<String>();
		sampleInput.add("100 5");
		sampleInput.add("PKG1 50 30 OFR001");
		sampleInput.add("PKG2 75 125 OFR0008");
		sampleInput.add("PKG3 175 100 OFR003");
		sampleInput.add("PKG4 110 60 OFR002");
		sampleInput.add("PKG5 155 95 NA");
		sampleInput.add("2 70 200");
		
		float baseDeliveryCost = EverestHelper.getBaseDeliveryCost(sampleInput.get(0));
		assertEquals(100f, baseDeliveryCost);
		
		EverestPackage pkg1 = EverestHelper.buildPackage(sampleInput.get(1), baseDeliveryCost);
		EverestPackage pkg2 = EverestHelper.buildPackage(sampleInput.get(2), baseDeliveryCost);
		EverestPackage pkg3 = EverestHelper.buildPackage(sampleInput.get(3), baseDeliveryCost);
		EverestPackage pkg4 = EverestHelper.buildPackage(sampleInput.get(4), baseDeliveryCost);
		EverestPackage pkg5 = EverestHelper.buildPackage(sampleInput.get(5), baseDeliveryCost);
		
		List<EverestPackage> packages = new ArrayList<EverestPackage>();
		packages.add(pkg1);
		packages.add(pkg2);
		packages.add(pkg3);
		packages.add(pkg4);
		packages.add(pkg5);
		
		Shipment shipment = new Shipment(1, packages);
		
		assertEquals(565f, shipment.getShipmentWeight());
	}
	
	public void testGetMaxPackageDistance() throws Exception {
		List<String> sampleInput = new ArrayList<String>();
		sampleInput.add("100 5");
		sampleInput.add("PKG1 50 30 OFR001");
		sampleInput.add("PKG2 75 125 OFR0008");
		sampleInput.add("PKG3 175 100 OFR003");
		sampleInput.add("PKG4 110 60 OFR002");
		sampleInput.add("PKG5 155 95 NA");
		sampleInput.add("2 70 200");
		
		float baseDeliveryCost = EverestHelper.getBaseDeliveryCost(sampleInput.get(0));
		assertEquals(100f, baseDeliveryCost);
		
		EverestPackage pkg1 = EverestHelper.buildPackage(sampleInput.get(1), baseDeliveryCost);
		EverestPackage pkg2 = EverestHelper.buildPackage(sampleInput.get(2), baseDeliveryCost);
		EverestPackage pkg3 = EverestHelper.buildPackage(sampleInput.get(3), baseDeliveryCost);
		EverestPackage pkg4 = EverestHelper.buildPackage(sampleInput.get(4), baseDeliveryCost);
		EverestPackage pkg5 = EverestHelper.buildPackage(sampleInput.get(5), baseDeliveryCost);
		
		List<EverestPackage> packages = new ArrayList<EverestPackage>();
		packages.add(pkg1);
		packages.add(pkg2);
		packages.add(pkg3);
		packages.add(pkg4);
		packages.add(pkg5);
		
		Shipment shipment = new Shipment(1, packages);
		
		assertEquals(125f, shipment.getMaxPackageDistance());
	}
	
	public void testGetPackages() throws Exception {
		List<String> sampleInput = new ArrayList<String>();
		sampleInput.add("100 5");
		sampleInput.add("PKG1 50 30 OFR001");
		sampleInput.add("PKG2 75 125 OFR0008");
		sampleInput.add("PKG3 175 100 OFR003");
		sampleInput.add("PKG4 110 60 OFR002");
		sampleInput.add("PKG5 155 95 NA");
		sampleInput.add("2 70 200");
		
		float baseDeliveryCost = EverestHelper.getBaseDeliveryCost(sampleInput.get(0));
		assertEquals(100f, baseDeliveryCost);
		
		EverestPackage pkg1 = EverestHelper.buildPackage(sampleInput.get(1), baseDeliveryCost);
		EverestPackage pkg2 = EverestHelper.buildPackage(sampleInput.get(2), baseDeliveryCost);
		EverestPackage pkg3 = EverestHelper.buildPackage(sampleInput.get(3), baseDeliveryCost);
		EverestPackage pkg4 = EverestHelper.buildPackage(sampleInput.get(4), baseDeliveryCost);
		EverestPackage pkg5 = EverestHelper.buildPackage(sampleInput.get(5), baseDeliveryCost);
		
		List<EverestPackage> packages = new ArrayList<EverestPackage>();
		packages.add(pkg1);
		packages.add(pkg2);
		packages.add(pkg3);
		packages.add(pkg4);
		packages.add(pkg5);
		
		Shipment shipment = new Shipment(1, packages);
		
		List<EverestPackage> shipmentPackages = shipment.getPackages(); 
		for(EverestPackage pkg: shipmentPackages) {
			assertTrue(pkg instanceof EverestPackage);
		}
	}
}