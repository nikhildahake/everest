package engineering.everest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import engineering.everest.helpers.EverestHelper;
import engineering.everest.helpers.EverestPackage;
import engineering.everest.helpers.Fleet;
import junit.framework.TestCase;

public class EverestLogisticsTest extends TestCase{
	public void testProgramInit() throws Exception {
		EverestLogistics el = new EverestLogistics();
	}
	
	public void testProblem1() {
		List<String> sampleInput = new ArrayList<String>();
		sampleInput.add("100 3");
		sampleInput.add("PKG1 5 5 OFR001");
		sampleInput.add("PKG2 15 5 OFR002");
		sampleInput.add("PKG3 10 100 OFR003");
		
		EverestLogistics el = new EverestLogistics();
		
		float baseDeliveryCost = EverestHelper.getBaseDeliveryCost(sampleInput.get(0));
		assertEquals(100f, baseDeliveryCost);
		
		int numPackages = EverestHelper.getNumPackages(sampleInput.get(0));
		assertEquals(3, numPackages);
		
		EverestPackage pkg1 = EverestHelper.buildPackage(sampleInput.get(1), baseDeliveryCost);
		EverestPackage pkg2 = EverestHelper.buildPackage(sampleInput.get(2), baseDeliveryCost);
		EverestPackage pkg3 = EverestHelper.buildPackage(sampleInput.get(3), baseDeliveryCost);
		
		String output1 = EverestHelper.getPackageCostOutput(pkg1);
		assertEquals("PKG1 0.0 175.0", output1);
		
		String output2 = EverestHelper.getPackageCostOutput(pkg2);
		assertEquals("PKG2 0.0 275.0", output2);
		
		String output3 = EverestHelper.getPackageCostOutput(pkg3);
		assertEquals("PKG3 35.0 665.0", output3);
	}
	
	public void testProblem2() {
		List<String> sampleInput = new ArrayList<String>();
		sampleInput.add("100 5");
		sampleInput.add("PKG1 50 30 OFR001");
		sampleInput.add("PKG2 75 125 OFFR0008");
		sampleInput.add("PKG3 175 100 OFFR003");
		sampleInput.add("PKG4 110 60 OFFR002");
		sampleInput.add("PKG5 155 95 NA");
		sampleInput.add("2 70 200");
		
		EverestLogistics el = new EverestLogistics();
		
		float baseDeliveryCost = EverestHelper.getBaseDeliveryCost(sampleInput.get(0));
		assertEquals(100f, baseDeliveryCost);
		
		int numPackages = EverestHelper.getNumPackages(sampleInput.get(0));
		assertEquals(5, numPackages);
		
		EverestPackage pkg1 = EverestHelper.buildPackage(sampleInput.get(1), baseDeliveryCost);
		EverestPackage pkg2 = EverestHelper.buildPackage(sampleInput.get(2), baseDeliveryCost);
		EverestPackage pkg3 = EverestHelper.buildPackage(sampleInput.get(3), baseDeliveryCost);
		EverestPackage pkg4 = EverestHelper.buildPackage(sampleInput.get(4), baseDeliveryCost);
		EverestPackage pkg5 = EverestHelper.buildPackage(sampleInput.get(5), baseDeliveryCost);
		
		List<EverestPackage> packages = new ArrayList<EverestPackage>();
		packages.add(pkg1);
		packages.add(pkg2);
		packages.add(pkg3);
		packages.add(pkg4);
		packages.add(pkg5);
		
		Fleet fleet = EverestHelper.getFleet(sampleInput.get(6));
		assertEquals(2, fleet.getNumOfVehicles());
		assertEquals(70.0f, fleet.getMaxSpeed());
		assertEquals(200.0f, fleet.getMaxWeight());
		
		HashMap<String, Float> packageIdDeliveryTimes = el.getPackageDeliveryTime(packages, fleet);
		
		String output1 = EverestHelper.getPackageCostAndDeliveryTimeOutput(pkg1, packageIdDeliveryTimes);
		assertEquals("PKG1 0.0 750.0 3.98", output1);
		
		String output2 = EverestHelper.getPackageCostAndDeliveryTimeOutput(pkg2, packageIdDeliveryTimes);
		assertEquals("PKG2 0.0 1475.0 1.78", output2);
		
		String output3 = EverestHelper.getPackageCostAndDeliveryTimeOutput(pkg3, packageIdDeliveryTimes);
		assertEquals("PKG3 0.0 2350.0 1.42", output3);
		
		String output4 = EverestHelper.getPackageCostAndDeliveryTimeOutput(pkg4, packageIdDeliveryTimes);
		assertEquals("PKG4 105.0 1395.0 0.85", output4);
		
		String output5 = EverestHelper.getPackageCostAndDeliveryTimeOutput(pkg5, packageIdDeliveryTimes);
		assertEquals("PKG5 0.0 2125.0 4.19", output5);
	}
}