package engineering.everest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import engineering.everest.helpers.EverestHelper;
import engineering.everest.helpers.EverestPackage;
import engineering.everest.helpers.Fleet;
import junit.framework.TestCase;

public class EverestHelperTest extends TestCase{
	
	public void testGetBaseDeliveryCost() throws Exception {
		float baseDeliveryCost = EverestHelper.getBaseDeliveryCost("100 3");
		assertEquals(100f, baseDeliveryCost);
	}
	
	public void testGetNumberOfPackages() throws Exception {
		int numPackages = EverestHelper.getNumPackages("100 3");
		assertEquals(3, numPackages);
	}
	
	public void testBuildPackage() throws Exception {
		EverestPackage pkg = EverestHelper.buildPackage("PKG1 5 5 OFR001", 100);
		assertEquals("PKG1", pkg.getPackageName());
		assertEquals(5f, pkg.getPackageWeight());
		assertEquals(5f, pkg.getPackageDistance());
		assertEquals("OFR001", pkg.getPackageOfferCode());
		assertEquals(0f, pkg.getPackageDiscount());
		assertEquals(175f, pkg.getPackageCost());
	}
	
	public void testGetPackageCostOutput() throws Exception {
		String output = EverestHelper.getPackageCostOutput(EverestHelper.buildPackage("PKG1 5 5 OFR001", 100));
		assertEquals("PKG1 0.0 175.0", output);
	}
	
	public void testGetFleet() throws Exception {
		Fleet fleet = EverestHelper.getFleet("2 70 200");
		assertEquals(2, fleet.getNumOfVehicles());
		assertEquals(70f, fleet.getMaxSpeed());
		assertEquals(200f, fleet.getMaxWeight());
	}
	
	public void testGetPackageCostAndDeliveryTimeOutput() {
		List<EverestPackage> packages = new ArrayList<EverestPackage>();
		EverestPackage pkg = EverestHelper.buildPackage("PKG1 50 30 OFR001", 100);
		packages.add(pkg);
		Fleet fleet = EverestHelper.getFleet("2 70 200");
		
		HashMap<String, Float> packageIdDeliveryTimes = new EverestLogistics().getPackageDeliveryTime(packages, fleet);
		assertEquals("PKG1 0.0 750.0 0.42", EverestHelper.getPackageCostAndDeliveryTimeOutput(pkg, packageIdDeliveryTimes));
	}
}