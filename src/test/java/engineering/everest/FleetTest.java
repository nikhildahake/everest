package engineering.everest;

import engineering.everest.helpers.EverestHelper;
import engineering.everest.helpers.Fleet;
import junit.framework.TestCase;

public class FleetTest extends TestCase{
	
	public void testGetNumOfVehicles() throws Exception {
		Fleet fleet = EverestHelper.getFleet("2 70 200");
		assertEquals(2, fleet.getNumOfVehicles());
	}
	
	public void testGetMaxSpeed() throws Exception {
		Fleet fleet = EverestHelper.getFleet("2 70 200");
		assertEquals(70f, fleet.getMaxSpeed());
	}
	
	public void testGetMaxWeight() throws Exception {
		Fleet fleet = EverestHelper.getFleet("2 70 200");
		assertEquals(200f, fleet.getMaxWeight());
	}
}