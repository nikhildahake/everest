package engineering.everest;

import engineering.everest.helpers.EverestHelper;
import engineering.everest.helpers.EverestPackage;
import junit.framework.TestCase;

public class EverestPackageTest extends TestCase{
	
	public void testGetPackageName() throws Exception {
		EverestPackage pkg = EverestHelper.buildPackage("PKG1 5 5 OFR001", 100);
		assertEquals("PKG1", pkg.getPackageName());
	}
	
	public void testGetPackageWeight() throws Exception {
		EverestPackage pkg = EverestHelper.buildPackage("PKG1 5 5 OFR001", 100);
		assertEquals(5f, pkg.getPackageWeight());
	}
	
	public void testGetPackageDistance() throws Exception {
		EverestPackage pkg = EverestHelper.buildPackage("PKG1 5 5 OFR001", 100);
		assertEquals(5f, pkg.getPackageDistance());
	}
	
	public void testGetPackageOfferCode() throws Exception {
		EverestPackage pkg = EverestHelper.buildPackage("PKG1 5 5 OFR001", 100);
		assertEquals("OFR001", pkg.getPackageOfferCode());
	}
	
	public void testGetPackageDiscount() throws Exception {
		EverestPackage pkg = EverestHelper.buildPackage("PKG1 5 5 OFR001", 100);
		assertEquals(0f, pkg.getPackageDiscount());
	}
	
	public void testGetPackageCost() throws Exception {
		EverestPackage pkg = EverestHelper.buildPackage("PKG1 5 5 OFR001", 100);
		assertEquals(175f, pkg.getPackageCost());
	}
}