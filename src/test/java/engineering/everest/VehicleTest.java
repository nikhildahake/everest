package engineering.everest;

import engineering.everest.helpers.Vehicle;
import junit.framework.TestCase;

public class VehicleTest extends TestCase {
	public void testGetNextAvailableAtHours() {
		Vehicle v = new Vehicle(1, 2);
		assertEquals(2f, v.getNextAvailableAtHours());
	}
	
	public void testSetNextAvailableAtHours() {
		Vehicle v = new Vehicle(1, 2);
		v.setNextAvailableAtHours(5);
		assertEquals(5f, v.getNextAvailableAtHours());
	}
}