Welcome to Everest Courier Service
==================================

This repository contains the code for Everest Courier Service problem. This read-me assumes
that you have access to the PDF which details out this problem.

1. Clone the repository using: git clone https://nikhildahake@bitbucket.org/nikhildahake/everest.git

2. Ensure Java 17 is installed and configured correctly for your system.

3. Import repository using eclipse (Version: 2022-03 (4.23.0)). Build the project.

4. To run problem 1, right click Problem1.java inside engineering.everest package and run as Java application. To
see sample run, see image problem1.png.

5. To run problem 2, right click Problem2.java inside engineering.everest package and run as Java application. To
see sample run, see image problem2.png.

6. Tests are located inside source package: src/test/java/. To run tests, right-click on the test you wish
to run and click on run as JUnit test.